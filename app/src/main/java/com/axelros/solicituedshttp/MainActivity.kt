package com.axelros.solicituedshttp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import okhttp3.Call
import okhttp3.OkHttpClient
import java.io.IOException
import java.lang.Exception
import java.security.spec.ECField

class MainActivity : AppCompatActivity(), CompletedListener {
    override fun downloadCompleted(result: String?) {
        Log.d("descargaCompleta", result)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bValdiateButton = findViewById<Button>(R.id.btnValidateNetwork)
        val bSolicitudHTTP = findViewById<Button>(R.id.bSolicitudHTTP)
        val bVolley = findViewById<Button>(R.id.bVolley)
        val bOKHTTP = findViewById<Button>(R.id.bOKHTTP)

        bValdiateButton.setOnClickListener {
            //Validate network
            if(Network.validateNetwork(this)){
                Toast.makeText(this, "There is netwrok connection", Toast.LENGTH_LONG). show()
            }
            else{
                Toast.makeText(this, "There is not network connection", Toast.LENGTH_LONG). show()
            }
        }

        bSolicitudHTTP.setOnClickListener {
            if(Network.validateNetwork(this)){
                //Log.d("bSolicitudOnClick",downloadData("http://www.google.com"))
                DownloadUrl(this).execute("https://www.google.com")
            }else{
                Toast.makeText(this, "There is not network connection", Toast.LENGTH_LONG). show()
            }
        }

        bVolley.setOnClickListener {
            if(Network.validateNetwork(this)){
                solicitudHTTPVolley("https://www.google.com")
            }else{
                Toast.makeText(this, "There is not network connection", Toast.LENGTH_LONG). show()
            }
        }

        bOKHTTP.setOnClickListener {
            if(Network.validateNetwork(this)){
                solicitudHTTPOkHTTP("https://www.google.com")
            }else{
                Toast.makeText(this, "There is not network connection", Toast.LENGTH_LONG). show()
            }
        }
    }

    //Volley method
    private fun solicitudHTTPVolley(url :String){
        Log.d("ESTEM DINS", "dins")
        val queue = Volley.newRequestQueue(this)
        val request = StringRequest(Request.Method.GET, url, Response.Listener<String>{
            response ->
            try {
                Log.d("solicitudHTTPVolley", response)
            }catch (e: Exception){
                Log.d("PORQUE", e.message)
            }
        }, Response.ErrorListener {  })

        queue.add(request)
    }

    //OKHTTTP
    private fun solicitudHTTPOkHTTP(url:String){
        val client = OkHttpClient()
        val request = okhttp3.Request.Builder().url(url).build()

        client.newCall(request).enqueue(object: okhttp3.Callback{
            override fun onFailure(call: Call?, e: IOException?) {
                //Implement error
            }

            override fun onResponse(call: Call?, response: okhttp3.Response) {
                val result = response.body().string()

                this@MainActivity.runOnUiThread {
                    try {
                        Log.d("solicitudHTTPOkHTTP", result)
                    }catch (e: Exception){

                    }
                }
            }
        })

    }

}

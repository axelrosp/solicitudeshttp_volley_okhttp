package com.axelros.solicituedshttp

interface CompletedListener {
    fun downloadCompleted(result: String?)
}